/*
 * Pleiar.no FEST-data extraction tool
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* global module process */
/* eslint-disable flowtype/require-return-type */
/* eslint-disable flowtype/require-variable-type */
/* eslint-disable flowtype/require-valid-file-annotation */
/* eslint-disable require-jsdoc */
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');

module.exports = {
    target: 'node',
    externals: [ nodeExternals() ],
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
        ]
    },
    plugins: [
        new webpack.BannerPlugin({ banner:"Copyright (C) Fagforbundet 2020\n\nThis program is free software: you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation, either version 3 of the License, or\n(at your option) any later version.\n\nThis program is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License\nalong with this program.  If not, see <http://www.gnu.org/licenses/>."}),
        new webpack.BannerPlugin({ banner:"#!/usr/bin/env node", raw: true }),
    ]
};

process.env.BABEL_ENV = 'node';
