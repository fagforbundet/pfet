/*
 * Pleiar.no FEST-data extraction tool
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import * as cheerio from 'cheerio';
import fs from 'fs';
import assert from 'assert';

import type { intermediateDrugEntryList, intermediateDrugEntryType, drugEntryList, drugEntryType, exchangeGroupIndex, drugExchangeList, FESTResultData, FESTResultFormat } from './types.js';

/**
 * A very simple FEST-parser. It reads the XML and processes that
 */
class sFestParser
{
    file: string;

    /**
     * Parse a single FEST file
     *
     * @param {string} file - The path to the XML file to load
     */
    constructor (params: {| file: string |})
    {
        this.file = params.file;
        if (!fs.existsSync(this.file))
        {
            throw(this.file+': does not exist');
        }
    }

    /**
     * Internal method that parses a list of drugs out of the FEST data and
     * returns a parsed array of drugEntryType
     */
    _parsePakning (C: typeof cheerio.Cheerio): intermediateDrugEntryList
    {
        const drugs: intermediateDrugEntryList = [];

        /**
         * Datastructure that we use to deduplicate medications with multiple
         * package sizes
         */
        const pakningDeduplicate: {
            [string]: number
        } = {};

        C('OppfLegemiddelpakning').each(function (this: typeof cheerio.Cheerio)
        {
            /** The current package as a cheerio object */
            const pakning = C(this);

            /* The various elements that we're going to use */
            const NavnFormStyrke = pakning.find('NavnFormStyrke');
            const name = NavnFormStyrke.text();
            const RefByttegruppe = pakning.find('RefByttegruppe');
            const atcElement = pakning.find('Atc');
            const atcCode = atcElement.attr('V');
            const PreparattypeElement = pakning.find('Preparattype');
            const Preparattype = PreparattypeElement.attr('DN');

            // Make sure we have the expected state
            assert.equal(pakning.length, 1,'Should only get a single pakning');
            assert.equal(NavnFormStyrke.length,1,'Should only have a single name');
            assert(RefByttegruppe.length <= 1,name+': should only have a single generica group, at most');
            assert.equal(PreparattypeElement.length,1,'Should have a single PreparattypeElement');
            assert(Preparattype.length > 0,'Should have a Preparattype text');

            // If we have no ATC code for the element, then it's not a drug and won't have
            // generica. We simply skip it.
            if(atcElement.length === 0)
            {
                return;
            }
            // If it has the special ATC code "WITHOUT" then it's also not a drug and won't have
            // generica. We skip this too.
            if (atcCode === "WITHOUT")
            {
                return;
            }

            // Ignore certain types of drugs
            if(/^(Vet legemiddel|Krever godkj. Fritak|Vet vaksine)$/.test(Preparattype))
            {
                return;
            }
            else if (/^(Unntak MT)$/.test(Preparattype))
            {
                console.log('Temporarily ignoring '+name+' because Preparattype='+Preparattype);
                return;
            }
            else if (! /^(Legemiddel|NAF-preparat|Sykehuspreparat|Medisinsk gass|Magistrell|Vaksine|Tradisjonelt plantebasert|Radiofarmaka|Veletablert plantebasert|Kosttilskudd)$/.test(Preparattype))
            {
                // $FlowExpectedError[incompatible-call] the typing for assert.fail is wrong, it has not been updated for the new API
                assert.fail(name+': '+Preparattype+' is an unhandled Preparattype');
            }

            // We expect a single ATC
            assert.equal(atcElement.length,1,name+': should only have a single ATC');
            assert(atcCode.length > 0,"Should have an ATC code");

            // Populate the drug datastruture
            const thisDrug: intermediateDrugEntryType = {
                name,
                atcCode,
                exchangeGroup: []
            };

            // Locate the exchange group, if present
            if(RefByttegruppe.length)
            {
                const exchangeGroup = RefByttegruppe.text();
                thisDrug.exchangeGroup.push(exchangeGroup);
            }

            /*
             * Perform the initial deduplication.
             *
             * The data in FEST contains multiple packages with the same name.
             * The difference between the packages are the number of tablets,
             * the amount of liquid etc.
             *
             * This isn't information that we need. Thus we try here to deduplicate it, so that
             * we only have one of each medication (here, medication refers to
             * a single medicine in a given strength in a given form, so
             * "Paracetamol 500mg" and "Paracetamol 1g" are still different)
             */
            const deduplicateID = pakningDeduplicate[name];
            // If there's another medicine seen already with the same name, perform the deduplication
            if(deduplicateID !== undefined)
            {
                const deduplicatedEntry = drugs[ deduplicateID ];
                // Verify that the ATC codes are the same. It's not really
                // expected that this will ever fail, but is provided as a
                // failsafe.
                if(deduplicatedEntry.atcCode !== atcCode)
                {
                    console.log(JSON.stringify(deduplicatedEntry));
                    console.log(JSON.stringify(thisDrug));
                    throw('Multiple generica with same name with different ATC codes! Bailing');
                }
                /*
                 * If our current drug has exchangeGroups, append our
                 * exchangeGroup to the existing element, if it doesn't already
                 * have it. The duplication also duplicates exchangeGroups,
                 * since an exchangeGroup is a group of medication in a given
                 * package size, form and strength. We only care about form
                 * and strength. Thus we add all known exchangeGroups to the medication.
                 * These will, in turn, be deduplicated later by @{link
                 * sFestParser._deduplicateGenerica}.
                 */
                if(thisDrug.exchangeGroup.length > 0)
                {
                    const exchangeGroup = thisDrug.exchangeGroup.shift();
                    if(! deduplicatedEntry.exchangeGroup.includes(exchangeGroup))
                    {
                        deduplicatedEntry.exchangeGroup.push( exchangeGroup );
                    }
                }
                return;
            }

            const arrayIdx: number = drugs.push(thisDrug) -1;
            pakningDeduplicate[name] = arrayIdx;
        });

        return drugs;
    }

    /**
     * Internal method that parses a list of drug exchange groups out of the
     * FEST data and returns a parsed datastrcture of exchangeGroupIndex
     */
    _parseDrugExchangeList (C: typeof cheerio.Cheerio): exchangeGroupIndex
    {
        // The final list that we will return
        const drugExchangeList: exchangeGroupIndex = {};
        C('OppfByttegruppe').each((i,element) =>
        {
            /** The cheerio object representing a single OppfByttegruppe */
            const oppfByttegruppe = C(element);
            /** The status of the OppfByttegruppe */
            const status = oppfByttegruppe.find('Status');
            // Declare our assumption
            assert.equal(status.length,1,'Should have a single status');
            /** Ignore inactive items */
            if(status.attr('DN') === "Aktiv oppføring")
            {
                // A single OppfByttegruppe can in theory have multiple Byttegruppe, so
                // iterate over each.
                oppfByttegruppe.find('Byttegruppe').each((i, element) =>
                {
                    /** The cheerio object representing a single Byttegruppe */
                    const byttegruppe = C(element);
                    /** A (text) boolean specifying if this Byttegruppe has a
                      * notice attached to it */
                    const MerknadTilByttbarhet = byttegruppe.find('MerknadTilByttbarhet');
                    /** The Id of this Byttegruppe, which is used by the
                      * OppfLegemiddelpakning to reference a Byttegruppe */
                    const Id = byttegruppe.find('Id');
                    // Declare our assumptions
                    assert.equal(MerknadTilByttbarhet.length,1,'Should have a MerknadTilByttbarhet');
                    // We need to have an Id
                    assert.equal(Id.length,1,'Should have a Id');
                    const IdString = Id.text();
                    assert(IdString.length > 0,'Should have a Id with non-zero length');
                    drugExchangeList[IdString] = {};

                    // If we have a MerknadTilByttbarhet, include that in the data
                    if(sFestParser.textToBoolean(MerknadTilByttbarhet.text()))
                    {
                        const BeskrivelseByttbarhet = byttegruppe.find('BeskrivelseByttbarhet');
                        assert.equal(BeskrivelseByttbarhet.length,1,'Should have a single BeskrivelseByttbarhet');
                        drugExchangeList[IdString].merknad = BeskrivelseByttbarhet.text();
                    }
                    else
                    {
                        assert.equal(byttegruppe.find('BeskrivelseByttbarhet').length,0,'If there is no MerknadTilByttbarhet=true then there should be no BeskrivelseByttbarhet');
                    }
                });
            }
            else
            {
                console.log('Inactive status: '+status.attr('DN'));
            }
        });
        return drugExchangeList;
    }

    /**
     * Performs generica-group deduplication
     *
     * Some drugs have multiple generica-groups for different vareieties of
     * packages (ie. 10 tablets vs. 100 tablets = different group). We don't
     * care about that, so we deduplicate it here.
     *
     * This will also build a list of generica groups and how many members they
     * have.  It will call _removeLonelyGroups() to remove ones we don't need.
     */
    _deduplicateGenerica(drugEntries: intermediateDrugEntryList, exchangeIndex: exchangeGroupIndex): {| drugs: intermediateDrugEntryList, exchangeIndex: exchangeGroupIndex |}
    {
        /**
         * Keeps track of duplicates in a map of [duplicate]->[replacement] keys
         * */
        const deduplicated: { [string]: string } = {};
        /**
         * This keeps track of how many members each group has.
         * This is used later to remove groups that have 0 or 1 members.
         */
        const groupChildren: { [string]: Array<number> } = {};
        /**
         * Keeps track of our "primary" generica groups. This is used so that
         * we can assert that we're not deleting a group that another medication
         * has declared as its "primary" group, ensuring data consistency
         */
        const primaries: { [string]: Array<intermediateDrugEntryType> } = {};
        /**
         * Iterate over every drug
         */
        for(let drugID: number = 0; drugID < drugEntries.length; drugID++)
        {
            /** A single drug */
            const entry = drugEntries[drugID];
            const { name } = entry;
            const exchangeGroup = entry.exchangeGroup.slice(0).sort();
            // Set the drug entry for this drug, for use by later deduplication passes
            entry.drugID = drugID;
            /* If the drug doesn't have any generic groups, then there's
             * nothing to deduplicate */
            if(exchangeGroup.length === 0)
            {
                continue;
            }
            /*
             * Iterate over each exchange group, checking if any of this drugs
             * exchange groups have already been seen, and deleted. If that's
             * the case, then this drug group has already decided on which
             * exchangeGroup to use. In which case we push that group to the front
             * of the list, so that we also choose that group.
             *
             * This also checks if a group has been chosen as any other as a
             * primary.  In which case it pushes that to the front of the list,
             * so that we choose that one.
             */
            for(const group of exchangeGroup)
            {
                // If we've been deduplicated before, use the result from that
                if(deduplicated[group] !== undefined)
                {
                    exchangeGroup.unshift( deduplicated[group] );
                    break;
                }
                // If this group has already been selected as a primary, use that
                else if(primaries[group] !== undefined)
                {
                    exchangeGroup.unshift(group);
                    break;
                }
            }

            /** The primary group */
            const firstGroup: string = exchangeGroup.sort().shift();
            // Declare our assumptions
            assert(firstGroup !== undefined,'Should have a defined firstGroup');
            assert(firstGroup,'Should have a truthy firstGroup');
            assert(firstGroup.length > 0,'Should have a nonzero-length firstGroup');
            // Get the intermediateDrugEntryType of our firstGroup
            const firstGroupInfo = exchangeIndex[firstGroup];
            assert(firstGroupInfo !== undefined, 'Should have firstGroup that exists');
            // Declare this group as a primary group
            if(primaries[firstGroup] === undefined)
            {
                primaries[firstGroup] = [ entry ];
            }
            else
            {
                primaries[firstGroup].push( entry );
            }
            if (!Array.isArray(groupChildren[firstGroup]))
            {
                groupChildren[firstGroup] = [];
            }
            // Set our exchangeGroup to only be a single exchangeGroup
            entry.exchangeGroup = [ firstGroup ];
            // Store the groupChildren information
            groupChildren[firstGroup].push(drugID);
            // Handle exchangeGroups if this entry has more than the one that
            // we've decided is our firstGroup
            if(exchangeGroup.length > 0)
            {
                // Iterate over every entry in our exchangeGroup
                for(const secondaryGroup of exchangeGroup)
                {
                    // If this entry in the array is undefined, has already
                    // been deduplicated or is the same as our firstGroup
                    // (which can happen because we try to locate deduplication
                    // groups earlier)
                    if(secondaryGroup === undefined || deduplicated[secondaryGroup] || firstGroup === secondaryGroup)
                    {
                        continue;
                    }
                    // The exchangeGroup for this entry
                    const groupInfo = exchangeIndex[secondaryGroup];
                    /*
                     * Here we're verifying that two groups, that we think are
                     * identical, actually ARE identical. We stringify both and
                     * compare them.  We don't expect this to ever fail, but
                     * if it does then it breaks one of our assumptions, so
                     * we error out.
                     */
                    if(JSON.stringify(firstGroupInfo) !== JSON.stringify(groupInfo))
                    {
                        console.log(typeof(firstGroupInfo));
                        console.log(firstGroup+'='+JSON.stringify(firstGroupInfo));
                        console.log(typeof(groupInfo));
                        console.log(secondaryGroup+'='+JSON.stringify(groupInfo));
                        throw(name+': multiple equivalent generica with different exchange groups, bailing');
                    }
                    // If some other drug has this ID as its primary group, perform deduplication by
                    // re-assigning it to this group.
                    if (primaries[secondaryGroup] !== undefined)
                    {
                        for(const subGroupEntry of primaries[secondaryGroup])
                        {
                            // If the ATC code doesn't match then we're trying
                            // to merge two drugs that aren't equal.
                            assert( subGroupEntry.atcCode === entry.atcCode, 'ATC codes of equivalent meds should match');
                            // Make sure we have a drugID for the entry
                            assert(subGroupEntry.drugID !== undefined, 'New subgroup should have a drugID');
                            assert(typeof(subGroupEntry.drugID) === 'number', 'New subgroup should have a drugID that is a number');
                            // Exchange the groups it has currently for this group
                            subGroupEntry.exchangeGroup = [ firstGroup ];
                            // Add this entry to the child list so that it
                            // doesn't get removed by _removeLonelyGroups
                            // $FlowExpectedError[incompatible-call] Doesn't understand that we've validated above
                            groupChildren[firstGroup].push(subGroupEntry.drugID);
                        }
                        // Finally, delete the entry that declares this (the
                        // duplicate, currently secondaryGroup) group as a
                        // primary group
                        delete primaries[secondaryGroup];
                    }
                    // Validate that there are no entries in secondaryGroup that still have this as exchangeGroup
                    if(exchangeIndex[secondaryGroup] !== undefined && Array.isArray(exchangeIndex[secondaryGroup].drugIndexes))
                    {
                        for(const exchangeGroupID of exchangeIndex[secondaryGroup].drugIndexes)
                        {
                            const exEntry = drugEntries[exchangeGroupID];
                            assert(exEntry.exchangeGroup.includes(secondaryGroup) === false,'No groups should still be using this as the primary group');
                        }
                    }
                    // Declare this group as deduplicated, and that firstGroup is replacing it
                    deduplicated[secondaryGroup] = firstGroup;
                    // Delete it
                    delete exchangeIndex[secondaryGroup];
                }
            }
            // Declare our assumptions
            assert(deduplicated[firstGroup] === undefined,'Should not have deduplicated the firstGroup');
            assert(exchangeIndex[firstGroup] !== undefined,'firstGroup should still exist');
        }
        this._removeLonelyGroups(groupChildren, exchangeIndex, drugEntries);
        return { drugs: drugEntries, exchangeIndex };
    }

    /**
     * Removes groups in exchangeIndex that only have 0 or 1 members.
     *
     * NOTE: This modifies exchangeIndex and drugs directly, not a copy.
     */
    _removeLonelyGroups(groupChildren: { [string]: Array<number> }, exchangeIndex: exchangeGroupIndex, drugs: intermediateDrugEntryList)
    {
        const entries = Object.keys(exchangeIndex);
        for(const groupEntry of entries)
        {
            const children = groupChildren[groupEntry];
            if (children.length <= 1)
            {
                const child = children.shift();
                if(child !== undefined)
                {
                    drugs[child].exchangeGroup = [];
                }
                delete exchangeIndex[groupEntry];
            }
        }
    }

    /**
     * Performs a re-indexing of a set of drugEntryList and exchangeGroupIndex
     * converting the exchangeGroupIndex into a exchangeGroupList, and converting
     * the `exchangeGroup` property of the drugEntryList from a string (which
     * refers to the exchangeGroupIndex) to an integer (which refers to the
     * exchangeGroupList)
     */
    _reindexLists (drugEntries: intermediateDrugEntryList, exchangeIndex: exchangeGroupIndex): FESTResultData
    {
        // This is the new exchange list that we will build
        const exchangeList: drugExchangeList = [];
        // This is the new drugEntryList that we will build
        const drugList: drugEntryList = [];
        // This is a temporary map which maps the old IDs (from
        // exchangeGroupIndex) to the new integer ID
        const remappedExchangeIDs: {
                [string]: number
            } = {};

        // This is an array of all IDs in our exchangeGroupIndex, which we iterate
        // over
        const exchanges = Object.keys(exchangeIndex);

        // Convert the old string IDs to a new numeric ID
        for(let i: number = 0; i < exchanges.length; i++)
        {
            // Retrieves the key, which is the ID of the entry
            const exchangeKey = exchanges[i];
            // Sets the array index `i` to the value of this drug exchange entry
            exchangeList[i] = exchangeIndex[ exchangeKey ];
            // Sets the internal map which maps the old exchange ID (string) to the
            // new exchange ID (number)
            remappedExchangeIDs[exchangeKey] = i;
        }

        // Change the drugEntryList entries ID from the string ID to the new
        // numeric ID
        for(let i: number = 0; i < drugEntries.length; i++)
        {
            // A single intermediateDrugEntryType
            const entry = drugEntries[i];
            // The new drugEntryType
            const newEntry: drugEntryType =  {
                name: entry.name
            };
            // Store the new entry in our final drugList
            drugList[i] = newEntry;
            // Retrieve our exchangeGroup
            const exchangeGroup = entry.exchangeGroup;
            assert(Array.isArray(exchangeGroup),'exchangeGroup should be an array');
            // Verify ATC
            assert(typeof(entry.atcCode) === 'string', 'ATC should be a string');
            assert(entry.atcCode.length >= 3, 'ATC should be at least >=3 in length: '+entry.atcCode);
            // If we don't have any exchangeGroup, then set atc and move to the next entry
            if(exchangeGroup.length === 0)
            {
                newEntry.atc = entry.atcCode;
                continue;
            }
            assert.equal(exchangeGroup.length,1,'exchangeGroup should have only a single entry');

            // Retrieve the exchangeGroup ID that we got from FEST
            const oldID = exchangeGroup.shift();
            assert( typeof(oldID) === 'string','The old ID should be a string');
            assert( exchangeIndex[oldID], 'The old ID ('+oldID+') should be a valid ID: '+JSON.stringify(entry));
            // Retrieve the new, numeric ID that we've built
            const newID = remappedExchangeIDs[oldID];
            assert( newID !== undefined,'The exchange group (for '+oldID+') should exist');
            assert( typeof(newID) === 'number', 'The new ID should be a number');
            // Set the exchangeGroup in the new data
            newEntry.exchangeGroup = newID;
            // Add ourselves to the list of drugs in this exchangeGroup
            if(! exchangeList[newID].drugIndexes)
            {
                exchangeList[newID].drugIndexes = [];
            }
            exchangeList[newID].atc = entry.atcCode;
            exchangeList[newID].drugIndexes.push(i);
        }

        return {
            drugs: drugList,
            exchangeList
        };
    }

    /**
     * Performs the actual parsing of the `file` provided to the constructor
     */
    parse (): FESTResultFormat
    {
        const C = cheerio.load(fs.readFileSync(this.file).toString(),{
            xmlMode: true
        });

        const drugs            = this._parsePakning(C);

        const drugExchangeList = this._parseDrugExchangeList(C);

        const deduplicated     = this._deduplicateGenerica(drugs,drugExchangeList);

        const converted        = this._reindexLists(deduplicated.drugs,deduplicated.exchangeIndex);

        return {
            ...converted,
            // The data format version written
            fVer: 1,
            // The data format version this version is backwards compatible with.
            // Given the following scenarion: in version Y a field gets added to the data.
            // Nothing else is changed. Thus version Y is simply version X with an extra field.
            // It can be used as a version X datastructure, one can simply ignore the new field.
            // Thus for an fVer=Y, fCompV would still be X. So parsers using format X could
            // look and see that fVer is higher than its version, but fCompV still specifies
            // that the data is backwards compatible, thus it can still use it.
            //
            // The intention with this field is to make it possible for a parser relying on the
            // new field to error out (by checking fVer), while an older parser
            // that doesn't rely on the new field, can check fCompV and know
            // that it can still continue, since it doesn't need the new field.
            //
            // Here's how to validate:
            // Given that fVer: 25, fCompV: 20, and our parser requires version 20:
            // Checks fVer, it is >20, so our test fails.
            // Checks fCompV, it is =20, so our test succeeds, we can use the file.
            //
            // Given the same, fVer:25, fCompV:20, and a parser requiring version 25:
            // Checks fVer, it is 25, so we can continue
            //
            // Again, given, fVer:25, fCompV: 20, and a parser requiring version 26:
            // Check fVer, it is below 26, so our test fails.
            // Doesn't check fCompV, since fCompV can never be > fVer
            fCompV: 0,
        };
    }

    /**
     * A method that converts a text boolean (the string "true" or "false")
     * into a proper boolean
     */
    static textToBoolean (text: string): boolean
    {
        if(text === "true")
        {
            return true;
        }
        else if(text === "false")
        {
            return false;
        }
        else
        {
            throw('textToBoolean() got non boolean text: '+text);
        }
    }
}

export { sFestParser };
