/*
 * Pleiar.no FEST-data extraction tool
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import { sFestParser } from './sFestParser.js';
import type { FESTResultFormat } from './types.js';
import fs from 'fs';

/**
 * Outputs a list of synonyms based upon provided data
 */
function outputSynonyms (list: FESTResultFormat, forIdx: number, forDrug: string, forDrugID: number)
{
    console.log('Synonymer for gruppe {'+forIdx+'} - ['+forDrugID+'] "'+forDrug+'":');
    const synonyms = list.exchangeList[forIdx];
    if(synonyms === undefined)
    {
        throw('Unable to lookup '+forIdx+' in synonym list');
    }
    if(! Array.isArray(synonyms.drugIndexes) || synonyms.drugIndexes.length <= 0)
    {
        throw('Drug has no synonyms');
    }
    for(const synonym of synonyms.drugIndexes)
    {
        const synDrug = list.drugs[synonym];
        if(synDrug === undefined)
        {
            throw('Unable to look up drug with ID '+synonym);
        }
        console.log('['+synonym+'] "'+ synDrug.name+'"' );
    }
}

/**
 * A function to do a quick, stupid, search through the PFET list
 */
function quickSearch (sResult: FESTResultFormat,str: string)
{
    const displayed: Array<boolean> = [];
    const searchStr: string = str.toLowerCase();
    let displayedSomething: boolean = false;
    for(let drugID: number = 0; drugID < sResult.drugs.length; drugID++)
    {
        const drug = sResult.drugs[drugID];
        const exchangeGroup = drug.exchangeGroup;
        if(drug.name.toLowerCase().indexOf(searchStr) !== -1)
        {
            if(exchangeGroup === undefined)
            {
                if(displayedSomething)
                {
                    console.log('');
                }
                displayedSomething = true;
                console.log('Legemiddel "'+drug.name+'" har ingen registrerte synonymer');
            }
            else if(typeof(drug.exchangeGroup) === 'number' && ! displayed[drug.exchangeGroup])
            {
                if(displayedSomething)
                {
                    console.log('');
                }
                outputSynonyms(sResult, exchangeGroup, drug.name, drugID);
                displayed[exchangeGroup] = true;
                displayedSomething = true;
            }
        }
    }
}

/**
 * Main entry point for PFET
 */
function main ()
{
    const parser = new sFestParser({
        file: './fest251.xml'
    });
    const result = parser.parse();
    fs.writeFileSync('./pfet.out.json',JSON.stringify(result));
    console.log('Wrote: ./pfet.out.json');
    if(process.argv[2])
    {
        console.log('');
        quickSearch(result, process.argv[2]);
    }
    process.exit(0);
}

main();

