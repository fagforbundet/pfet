/*
 * Pleiar.no FEST-data extraction tool
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

export type intermediateDrugEntryType = {|
    name: string,
    atcCode: string,
    exchangeGroup: Array<string>,
    drugID?: number
|};

export type intermediateDrugEntryList = Array<intermediateDrugEntryType>;


export type drugEntryType = {|
    name: string,
    atc?: string,
    exchangeGroup?: number
|};

export type drugEntryList = Array<drugEntryType>;

export type exchangeGroupIndexEntry = {|
    merknad?: string,
    drugIndexes?: Array<number>,
    atc?: string
|};

export type FESTResultData = {|
    drugs: drugEntryList,
    exchangeList: drugExchangeList,
|};

export type FESTResultFormat = {|
    drugs: drugEntryList,
    exchangeList: drugExchangeList,
    fVer: number,
    fCompV: number
|};

// FIXME: Should have its own datatype
export type drugExchangeList = Array<exchangeGroupIndexEntry>;

export type exchangeGroupIndex = {
    [string]: exchangeGroupIndexEntry
};

