# Pleiar.no FEST-data extraction tool
#
# Copyright (C) Fagforbundet 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

default: build

SHELL:=/bin/bash
YARN=$(shell if which yarnpkg &>/dev/null; then echo yarnpkg; else echo yarn;fi)
NODEJS?=$(shell if which nodejs &>/dev/null; then echo nodejs; else echo node;fi; if [ "$$CI" == "" ]; then echo "--enable-source-maps";fi)
WGET=$(shell if [ "$$CI" != "" ]; then echo "wget --no-verbose"; else echo "wget";fi)
PRENAME=$(shell if which prename &>/dev/null; then echo "prename"; elif which "file-rename"; then echo "file-rename";else echo "rename";fi)
YARN_RUN=$(YARN) run -s
WEBPACK_SCRIPT_COMPILE=$(YARN_RUN) webpack --mode=production --output-path $(PWD)

help: _help depCheck
# This target outputs a formatted list of all of the "public" targets in this Makefile
_help: FMT="%-18s%s\\n"
_help:
	@echo "PFET makefile targets:"
	@printf "$(FMT)" "build" "Builds a ready-to use version"
	@printf "$(FMT)" "run" "Builds and runs a ready-to use version"
	@printf "$(FMT)" "apidoc" "Builds the API documentation using documentation.js into apidocs/"
	@printf "$(FMT)" "test" "Runs all tests"

run: build
	$(NODEJS) ./.src-nodecompatible/index.mjs

download: PREINFO=$(shell if [ -e fest251.zip ]; then ls -l fest251.zip;fi)
download:
	rm -f fest251.xml
	$(WGET) -N 'https://www.legemiddelsok.no/_layouts/15/FESTmelding/fest251.zip'
	unzip fest251.zip
	[ -e fest251.xml ]

ciBuild: clean download run
	mkdir -p public
	mv pfet.out.json public/
	echo '<html><head><title>PFET data repo</title></head><body>This is the PFET data repo. See <a href="https://gitlab.com/fagforbundet/pfet">https://gitlab.com/fagforbundet/pfet</a></body></html>' > public/index.html
	echo -e "User-agent: *\nDisallow: /" > public/robots.txt
	gzip --keep public/*

# This does a (stupid) check for our main dependencies, namely that yarn is
# installed and that the node_modules directory insists.
depCheck:
	@if ! which $(YARN) &>/dev/null; then echo "Error: requires yarn to be installed. See https://yarnpkg.com/lang/en/docs/install/";exit 1;fi
	@if [ ! -e "./node_modules" ]; then echo "Error: You must install the pleiar.no dependencies. Run: $(YARN) install";exit 1;fi
	@if [ ! -e "./flow-typed" ]; then echo "Warning: flow typedefinitions (flow-typed) not installed";echo "Run $(YARN) flow-typed install to fix this.";fi

# Runs our meta description generation script to update the react-snap-written
# HTML pages with a page-specific meta description
build: nodejsCompatibleTree
nodejsCompatibleTree:
	rm -rf .src-nodecompatible
	mkdir -p .src-nodecompatible
	$(YARN_RUN) flow-remove-types --quiet src/ --out-dir ./.src-nodecompatible/
	perl -pi -e "s{\.js'}{\.mjs'}g;" $$(find .src-nodecompatible -iname '*.js')
	find .src-nodecompatible -iname '*.js'| $(PRENAME) 's/\.js$$/.mjs/'
# Builds our documentation
apidoc: depCheck data
	rm -rf apidocs
	-$(YARN_RUN) documentation build src/ -f html -o apidocs
# Clean up
# Note: should NOT remove data (fest251.xml and fest251.zip)
clean:
	rm -rf public/ .src-nodecompatible

# ===
# Tests
# ===

# Default parameters for jest
JEST_PARAMS?=
# The global test target
testNoFlow: depCheck jest eslint
test: testNoFlow flow
# Verify types with flow
flow:
	@if [ ! -d flow-typed ]; then $(YARN) flow-typed install;fi
	$(YARN_RUN) flow check --include-warnings  src
# Flow coverage report
flowCoverage: depCheck 
	$(YARN_RUN) flow-coverage-report -t html -i 'src/**/*.js'
# Verify syntax with eslint.
# For these tests, the indent rule is not important. For manually ran
# instances of eslint it is set as an error
eslint:
	@# Using shell echo to expand the glob before running the rule, so that
	@# it's obvious what eslint is processing.
	$(YARN_RUN) eslint --ignore-pattern node_modules src/ $(PLEIAR_ESLINT_PARAMS)
# Run testsuite
jest: data
	$(YARN_RUN) jest $(JEST_PARAMS)
snyk:
	snyk test --dev --file=yarn.lock || snyk wizard --dev --file=yarn.lock
