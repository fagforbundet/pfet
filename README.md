# Pleiar.no FEST-data extraction tool

This repository is a part of
[pleiar.no](https://gitlab.com/fagforbundet/pleiar.no) a set of tools for
nurses and other health care professionals created and maintained by the
Norwegian union [Fagforbundet](https://fagforbundet.no).

This is a partial implementation of [FEST, the Norwegian Electronic
Prescription Support
System](https://legemiddelverket.no/andre-temaer/fest/hvordan-bruke-fest), as
published by the [Norwegian Medicines Agency](https://legemiddelverket.no/). It
is based upon FEST 3.0.

The purpose of this data extraction tool is *not* to implement all of FEST. It
is specifically a tool that extracts information about generic medication from
FEST and provides that in a format that is fit for consumption by the main
pleiar.no application.

## Documentation

The FEST version 3.0 [implementation
guide](https://legemiddelverket.no/Documents/Andre%20temaer/FEST/Hvordan%20bruke%20FEST/Implementation%20guide%20FEST%20v3.0.pdf)
([in
norwegian](https://legemiddelverket.no/Documents/Andre%20temaer/FEST/Hvordan%20bruke%20FEST/20190206_Implementeringsveiledning%20FEST%20v3.0.pdf))
has been used to implement this tool.

## Notes about code style

The source code for this tool is littered with assertions. This is because
errors could be severe. It does not attempt to recover from errors or
invalid/corrupt/unexpected data. It exits. This is by design. If the tool
errors out on data that's valid, open up an issue. If you want to submit
patches you should follow the same style, adding assertions for any assumptions
that you make. We prioritize safety and correctness over speed.

## Data license

The FEST data that it consumes is used under the [Norwegian Licence for Open
Government Data (NLOD)](https://data.norge.no/nlod/en) ([license in
Norwegian](https://data.norge.no/nlod/no)). As such, any data output by the
tool, will also be licensed under this same license, as it is a modified
version of the original data. Any consumers must therefore comply with the
NLOD.

## Source code license

Copyright © Fagforbundet 2020

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
